# Update 0.0.2-dev-0
---
## GENERAL CHANGES: 

> + **[+]** Changed directory name for better recognition: ./core into ./lib
> + **[^]** Moved primary config to new location: ./etc
>   * **[+]** New filename for general config file: etc/neuralnebula.py
> + **[^]** Updated documentation:
>   * **[+]** General config file available in: [docs/lib.cfg.app.md](docs/lib.cfg.app.md)
>   * **[+]** Logging module documentation available in: [docs/lib.logger.md](docs/lib.logger.md)
>   * **[+]** Main documentation file added [docs/index.md](docs/index.md)

### [MODULE] Logging

> + [^] Updated definitions for logger
> + 
