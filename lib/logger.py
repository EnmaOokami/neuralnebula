### Changelog:
#--------------------------- 0.0.2
# > Initial commit
# > Added changelog comment section
###
import logging

from lib.cfg.app import AppName, AppRoot, DevMode
from lib.cfg.app import LOG_MessageFormat, AppEncoding
from lib.cfg.app import LOG_File, LOG_BotLogLocation, LOG_FileMaxSize, LOG_BackupCount, LOG_Compression
from lib.cfg.app import LOG_Console

from multiprocessing import Process, Queue
from threading import Thread
from sys import stdout

if DevMode == True:
    logLevel = logging.DEBUG
else:
    logLevel = logging.WARNING

log = logging.Logger(f'{AppName}', logLevel) # Arm logger

if LOG_File == True: # Check if logging to file is enabled, then add handler to logger
    _fileHandler = logging.FileHandler(LOG_BotLogLocation, 'a', encoding=AppEncoding)
    if DevMode == True:
        _fileHandler.setLevel(logging.DEBUG)
    else:
        _fileHandler.setLevel(logging.WARNING)
    _fileHandler.setFormatter(logging.Formatter(LOG_MessageFormat))      
    log.addHandler(_fileHandler)

if LOG_Console == True or DevMode == True: # Check if console display or debug mode is enabled, then add handler to logger
    _cliHandler = logging.StreamHandler(stdout)
    if DevMode == True:
        _cliHandler.setLevel(logging.DEBUG)
    else:
        _cliHandler.setLevel(logging.WARNING)
    _cliHandler.setFormatter(logging.Formatter(LOG_MessageFormat))
    log.addHandler(_cliHandler)

