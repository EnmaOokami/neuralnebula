### Changelog:
#--------------------------- 0.0.2
# > Initial commit
# > Added changelog comment section
###
from time import time
from lib.cfg.app import HostPlatform
from lib.logger import log
from bz2 import *
from os import path, listdir, mkdir, remove, rmdir, rename, getenv
from uuid import uuid4, Enum

from pathlib import Path as plp


""" Core functions used in application """
class NN_Core():
    def __init__(self, input_ = None, output = None, mode = None, list_ = None, *args):
        self.input_ = input_
        self.output = output
        self.mode = mode
        self.list = list_

    """ File interaction functions """
    class File:
        def __init__(self, filename):
            self.filename = filename
            
        def __call__(self):
            pass
        
        """ Generate path to file by schematic """
        def GeneratePath(self, schematic): 
            if path.exists(path.abspath(self.filename)):
                if path.isfile(path.abspath(self.filename)):
                    tDataSch = {
                        'path': path.dirname(path.abspath(self.filename)),
                        'filename': path.basename(self.filename),
                        'fileExt': path.splitext(self.filename)
                    }
                else: log.error(f'Object {self.filename} is not a file.')
            else: log.error(f'File or path to file {self.filename} does not exist.')
            self.schema = schematic.format_map(tDataSch)
            return self.schema


""" Utility functions """
class NN_Utils():
    def __init__(self):
        pass

    def __call__(self):
        pass

    class TimerError(Exception):
        def __call__(self, text):
            log.exception(text)

    class Timer:
        def __init__(self):
            self._start_time = None

        def start(self):
            if self._start_time is not None:
                log.error(f"Timer is running. Use .stop() to stop it")

            self._start_time = time.perf_counter()

        def stop(self):
            if self._start_time is None:
                log.error(f"Timer is not running. Use .start() to start it")

            elapsed_time = time.perf_counter() - self._start_time
            self._start_time = None
            log.info(f"Elapsed time: {elapsed_time:0.4f} seconds")
