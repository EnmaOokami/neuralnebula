### Changelog:
#--------------------------- 0.0.2
# > Initial commit
# > Added changelog comment section
###
import zlib as zl

from lib.logger import log
from pathlib import Path
from lib.cfg.app import LOG_Compression, LOG_BackupCount
from lib.cfg.app import AppRoot, AppEncoding
from lib.cfg.app import LOG_BotLogLocation

from time import time as t
from bz2 import BZ2File as bz2f
from json import JSONEncoder as js
from os import path as ph

_packResult = [] # If more than 1 file to archive, they are listed as List
_AppLogDestination = 'var/log/AppLogs' # Application archive destination directory
_MessageLogDestination = 'var/log/MsgLogs' # Messages archive destination directory
_destinationFile = None # Destination archive file
_sourceFile = None # Source file to archive

_CompressionLvl = 9

class bzmod():
    def __init__(self, inputFile = _sourceFile, outputFile = _destinationFile, fileMode = 'wb', compLevel = _CompressionLvl, encoding = AppEncoding, packer = 'zlib'):
        if ph.exists(ph.abspath(inputFile)) == True:
            self.sourceFile = inputFile
            log.info(f'Compression input file {ph.abspath(inputFile)} found.')
        else:
            log.error(f'File or path to file {inputFile} does not exist.')

        if ph.exists(ph.abspath(outputFile)) != True:    
            self.destFile = outputFile
            log.info(f'Archive output file {ph.abspath(outputFile)} registered.')
        else:
            log.error(f'File {outputFile} already exist!')

        self.fMode = fileMode
        self.compLvl = compLevel
        self.encoding = encoding
        self.method = packer
        
    def __call__(self):
        pass
        
    def create(self):
        readData = Path(self.sourceFile).read_bytes()
        compData = Path(self.destFile).read_bytes()
        compRatio = (float(len(readData) - float(len(compData)))) / float(len(readData))

        if self.method == 'bz2':
            init_bz2f = bz2f(self.destFile, self.fMode, any, self.compLvl)
            try:
                init_bz2f.write(readData)
            except ValueError as e:
                log.exception(e)
            except Exception as e:
                log.exception(e)
            finally:
                init_bz2f.close()

        elif self.method == 'zlib':
            init_zlib = zl.compress(readData, zl.Z_BEST_COMPRESSION)
            try:
                with open(self.destFile, self.fMode) as f:
                    f.write(init_zlib)
            except ValueError as e:
                log.exception(e)
            except Exception as e:
                log.exception(e)
            finally:
                f.close()
                log.info(f'Compressing file {self.sourceFile} finished.')



