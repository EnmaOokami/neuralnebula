### Changelog:
#--------------------------- 0.0.2
# > Initial commit
# > Added changelog comment section
###

"""
"! Bot Control Settings, please be carefull while making changes
"""
Token = 'NDYxMTQzNTQ4Njk2MTk5MTY5.XrgZ3Q.P7HV5I74QzIBZPSeTtTdRP2Lj1M' # Discord token
DiscordName = 'Wolf Rayet#7514' # (OPTIONAL) Bot discord name and hash
UserCommandPrefix = '!' # User access command prefix
AdminCommandPrefix = '~~' # Admin access command prefix

"""
"! Bot Main Settings, DO NOT MAKE CHANGES unless you know what you are doing
"! Public version does not contain script to clean up development mode, please be warry of that
"""
AppName = 'neuralnebula'
AppEncoding = 'UTF-8'

"""
"! Bot Logging Settings, DO NOT MAKE CHANGES unless you know what you are doing
"""
LOG_File = True
LOG_Console = False
LOG_Compression = True
LOG_FileMaxSize = int('10')
LOG_BackupCount = int('10')
LOG_BotLogLocation = 'var/error.log'
LOG_MessageFormat = '[%(levelname)s]:[%(name)s]:[%(asctime)s]:[%(filename)s]:[%(funcName)s] %(message)s'

LOG_MessageLogging = False
LOG_MessageRecLogLocation = 'var/messages' 
LOG_MHistoryTime = 90 # Days

"""
"! Bot SQL Settings, DO NOT MAKE CHANGES unless you know what you are doing
"""
SQL_CommandFile = 'var/data/commands.dat'
SQL_AdditionalConfigFile = 'var/data/appdb.dat'

"""
"! KINDLY PLEASE - DO NOT CHANGE "AppRoot". It will most likely mess up your program and make it unable to start.
"! KINDLY PLEASE - do not turn debug mode. It activates developer options which are used to find out errors. It generate massive amount of data in both - log file and console.
"""
# Application core settings
from os import path

DevMode = True
AppRoot = path.abspath(path.dirname(__file__))

# Check for platform
from sys import platform

if platform.startswith('freebsd'): #Free-BSD
    HostPlatform = 'freebsd'
elif platform.startswith('linux'): #Linux
    HostPlatform = 'linux'
elif platform.startswith('win32'): #Windows
    HostPlatform = 'win32'
elif platform.startswith('darwin'): #macOS
    HostPlatform = 'macos'
else: #Default = linux
    HostPlatform = 'linux' 

# Locale settings
from locale import LC_ALL

APP_Locale = LC_ALL