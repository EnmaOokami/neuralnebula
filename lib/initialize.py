### Changelog:
#--------------------------- 0.0.1
# > Initial commit
#--------------------------- 0.0.2
# > Added changelog comment section
# > Rebuilded confirmation on connection, now uses logging ability
###

from discord import Client
from lib.logger import log

# Create Client Instance
client = Client()
cLatency = client.latency

@client.event
async def on_ready():
    log.Info('Successfull connection to {0} as {1}', client.get_guild, client)
    log.info('Current connection latency: {0}', cLatency)

@client.event
async def on_message(message):
    if message.author == client.user:
        return
    if message.content.startswith('$hello'):
        await message.channel.send('Hello!')

#client.run()