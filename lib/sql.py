### Changelog:
#--------------------------- 0.0.1
# > Initial commit
#--------------------------- 0.0.2
# > Added changelog comment section
###

from config import SQL_AdditionalConfigFile, SQL_CommandFile
from os import path
import sqlite3

class _InitialSetup():
    def __init__(self, file, template = None):
        super().__init__()
        self.Filename = file
        self.TableName = f'_dborigin'
        if self.Filename == SQL_AdditionalConfigFile:
            self.TableRows = DBConfig.defaultConfigTable
        if self.Filename == SQL_CommandFile:
            self.TableRows = DBCommand.defaultCommandTable

    def _CheckFile(self):
        if path.exists(self.Filename):
            return True
        else:
            return False

    def _CreateFile(self):
        con = sqlite3.connect(self.Filename)
        c = con.cursor()
        c.execute(f'''CREATE TABLE {self.TableName} ({self.TableRows})''')
        con.commit()
        con.close()      
        
class DBCommand():
    # Bind db file
    dbFile = SQL_CommandFile
    # Command table schematic
    # Commands are grouped in tables
    defaultCommandTable = "id INTEGER UNIQUE PRIMARY KEY AUTOINCREMENT, uid BLOB, ugroup TEXT, trigger TEXT, message BLOB"

    def __init__(self):
        super().__init__()
        if _InitialSetup(self.dbFile)._CheckFile() == False:
            _InitialSetup(self.dbFile)._CreateFile()

    def AddTable(self, name):
        dbcon = sqlite3.connect(self.dbFile)
        cur = dbcon.cursor()
        cur.execute(f'''CREATE TABLE {name} ({self.defaultCommandTable})''')
        dbcon.commit()
        dbcon.close()

    def AddRecord(self, table, content):
        dbcon = sqlite3.connect(self.dbFile)
        cur = dbcon.cursor()
        cur.execute(f'''INSERT INTO {table} (uid, ugroup, trigger, message) VALUES ('{content['uid']}', '{content['ugroup']}', '{content['trigger']}', '{content['message']}')''')
        dbcon.commit()
        dbcon.close()

    def SelectRecord(self, table, trigger):
        t = (f'{trigger}',)
        dbcon = sqlite3.connect(self.dbFile)
        cur = dbcon.cursor()
        cur.execute(f'SELECT uid, ugroup, message FROM {table} WHERE trigger = ?', t)
        query = cur.fetchone()
        result = {
            'uid': query[0],
            'ugroup': query[1],
            'trigger': trigger,
            'message': query[2]
        }
        dbcon.close()                
        return result

    def RemoveRecord(self, table, trigger):
        t = (f'{trigger}',)
        dbcon = sqlite3.connect(self.dbFile)
        cur = dbcon.cursor()
        cur.execute(f'DELETE FROM {table} WHERE trigger = ?', t)
        dbcon.commit()
        dbcon.close()

    def EditRecord(self, table, id, content):
        pass

class DBConfig():
    # Bind db file
    dbFile = SQL_AdditionalConfigFile
    # Additional configurations schematic
    # Table name = script name
    defaultConfigTable = "id INTEGER UNIQUE PRIMARY KEY AUTOINCREMENT, key TEXT, value BLOB"

    def __init__(self):
        super().__init__()
        if _InitialSetup(self.dbFile)._CheckFile() == False:
            _InitialSetup(self.dbFile)._CreateFile()

    def AddTable(self, name):
        dbcon = sqlite3.connect(self.dbFile)
        cur = dbcon.cursor()
        cur.execute(f'''CREATE TABLE {name} ({self.defaultConfigTable})''')
        dbcon.commit()
        dbcon.close()

    def AddRecord(self, table, content):
        dbcon = sqlite3.connect(self.dbFile)
        cur = dbcon.cursor()
        cur.execute(f'''INSERT INTO {table} VALUES ('{content['key']}', '{content['value']}')''')
        dbcon.commit()
        dbcon.close()

    def SelectRecord(self, table, trigger):
        t = ('{trigger}',)
        dbcon = sqlite3.connect(self.dbFile)
        cur = dbcon.cursor()
        cur.execute(f'SELECT value FROM {table} WHERE key=?', t)
        query = cur.fetchone()
        dbcon.close()
        result = {
            'key': trigger,
            'value': query[0]
        }              
        return query

    def RemoveRecord(self, table, id):
        dbcon = sqlite3.connect(self.dbFile)
        cur = dbcon.cursor()
        cur.execute(f'''DELETE FROM {table} WHERE id = {id}''')
        dbcon.commit()
        dbcon.close()

    def EditRecord(self, table, id, content):
        pass

