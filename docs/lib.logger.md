# LOGGING MODULE

## Name: lib/logger.py
### Internal logging module

---

### Usage:

**Import: **
```python
from lib.logger import log
```

**Log levels: **
```python
log.debug('message') # DEBUG Level
log.info('message') # INFO level
log.warning('message') # WARNING level
log.error('message') # ERROR level
log.critical('message') # CRITICAL level
```

---

### Configuration:

**Log to file: **./config.py => LOG_File

    Note: Manage saving logs into file. Can be turned on or off.

    Options:
    | Option | Result |
    |--------|--------|
    | True   | Logging is turned on. All log messages will be wrote to file located in *LOG_BotLogLocation* location. |
    | False  | Logging is disabled. No logs wrote to file. NOT RECOMMENDED! |

    Default: True

**Logs into console: **./config.py => LOG_Console

    Note: Manage showing log messages on console which actually runs program. It is turned of by default when developer mode is active in config file.

    Options:
    | Option | Result |
    |--------|--------|
    | True   | Logging is turned on. All log messages will be showed on program running console.  NOT RECOMMENDED! |
    | False  | Logging is disabled. No logs will be showed. |

    Default: False

**Endpoint file: ** ./config.py => LOG_BotLogLocation

    Note: Expected path to log file. Should be placed inside '' string tags

    Default: 'var/error.log'

**Log message format: ** ./config.py => LOG_MessageFormat