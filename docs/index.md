# **Neural Nebula Discord Bot**

## **Documentation (in progress)**

1. Introduction
2. Installation
3. Configuration

---

### **File documentation (in progress)**

> /lib - *Application workflow libraries and core functions*

> cfg - *Configuration files*
> 
>   * \<name\>.py - Python3 importable config file. Import single variables!
>   * \<name\>.conf - Standard UNIX configuration file.
>   * \<name\>.json - JSON config file

  
  * [Doc-Link] [app.py](lib.cfg.app.md) - *Main configuration file, user should edit it before running server
  * logger.conf - *Logger additional configuration*

* bzmod.py - *BZ2 Compression module*
* cli.py - *Console STDOUT*
* core.py - *Core methods and classes for application*
* extDeco.py - *Decorators*
* initialize.py - ***TEMPORARY** Init file*
* [Doc-Link] [logger.py](lib.logger.md) - *Logger module*
* message.py - *Discord message IN/OUT and processing module*
* sql.py - *SQLite v3 module*

> /var - *Usage files, logs, databases, etc.*

> /var/data - *Database and binary files*

  * [SQLite-DB] appdb.dat - *Main application database, contain additional settings for modules, user list, group list, etc.
  * [SQLite-DB] commands.dat - *User defined commands*

> /var/log - *Compressed backup logs of all kinds*
>
> app - Compressed application logs storage
> 
> msg - Compressed message recording files
> 
>       * File: <CategoryDir>/<FileName>.XX.gz [XX += 01]
>       * Format: Compressed BZ2
>       * CRC: YES (crc.list.json)
> 

* crc.list.json - *List of CRC hashes for all files*

> /var/messages - *Discord chat recordings*
> 
>       * File: <Server_ID>-<Channel_ID>.msg.log
>       * Format: Plain text
>       * Compression: YES (bz2)
>       * Archive: YES (user set amount of days)
> 

---
