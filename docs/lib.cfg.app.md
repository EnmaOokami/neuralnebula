# CONFIGURATION FILES

## Name: lib/cfg/app.py
### Configuration file explanation

Configuration file contain most of user configurable options regarding application. Every setting is a Python 3 variable/key-value dictionary. Below you can find explanation of every configurable option with default values.

---
### Basic settings - MUST BE SET UP BEFORE RUNNING PROGRAM

> Bot configuration

| Variable Name     | Default       | Format    | Description | Example |
|-------------------|---------------|-----------|-------------|---------|
| Token | null | String | Your discord bot token, please go to [Discord Developer Portal](https://discord.com/developers) and after creating new bot, paste token here. | - |
| DiscordName | null | String | Your discord bot name with hash number. Optional. | 'MyBot#0000' |
| UserCommandPrefix | '!' | String | User executable command, used as default prefix for bot commands. | !command |
| AdminCommandPrefix | '\~\~' | String | Admin executable command, used as default prefix for users with admin rights. Used only for administration commands. | \~\~useradd |

---
### Additional settings - Change if you know what you are doing

> Application settings

| Variable Name     | Default       | Format    | Description | Example |
|-------------------|---------------|-----------|-------------|---------|
| AppName | 'neuralnebula' | String | Application name, in case you would like to change it | 'My Bot Program' |
| AppEncoding | 'UTF-8' | String | Set text encoding format. Please check [Standard Encodings Doc](https://docs.python.org/3/library/codecs.html#standard-encodings) for more information and list of possible encodings. | 'ascii' |

> Logging settings

| Variable Name     | Default       | Format    | Description | Example |
|-------------------|---------------|-----------|-------------|---------|
| LOG_File | True | Boolean | Turn on/off writing logs to file. Application wide range! | - |
| LOG_Console | False | Boolean | Turn on/off showing logs in console where application is running. By default is off due to possible high amount of logs messages and console spam. | - |
| LOG_Compression | True | Boolean | Turn on file compression. Utilize separate thread to constantly watch over log files size and execute compression when they exceed user specified size. | - |
| LOG_FileMaxSize | int('10')  | Str->Int | User specified maximum log file size. **Format: Megabytes** |  int('25') |
| LOG_BackupCount | int('10') | Str->Int | Amount of rotations for log file. | int('5') |
| LOG_BotLogLocation | 'var/error.log' | String | Internal location path where log file will be written. Path should contain log filename and extension. If directory or file is not found, program will exit with error. | 'log/mylogs.log' |
| LOG_MessageFormat | '[%(levelname)s]:[%(name)s]:[%(asctime)s]:[%(filename)s]:[%(funcName)s] %(message)s' | String | Configure log format. Please go to [LogRecord attributes Manual Page](https://docs.python.org/3/library/logging.html#logrecord-attributes) for more information about log message structure. | '%(asctime)s - %(message)s' |
| LOG_MessageLogging | False | Boolean | Activate writing all outgoing and incoming messages to file. **Note: This cause performance issue!** | - |
| LOG_MessageRecLogLocation | 'var/messages' | String | Internal location path where message file will be saved. Please be aware that file is saved as plain text and if compression is turned off, it size will depends on server activity where bot is logged on. | 'messages/chat.log' |
| LOG_MHistoryTime | 90 | Int | Amount of days for what message log is kept. **Format: days** | 30 |

> Database

| Variable Name     | Default       | Format    | Description | Example |
|-------------------|---------------|-----------|-------------|---------|
| SQL_CommandFile | 'var/data/commands.dat' | String | Default command database file which contain all preloaded commands. **It is recommended to not make changes unless you generated new file!** | 'mydb/appcommand.dat' |
| SQL_AdditionalConfigFile | 'var/data/appdb.dat' | String | Default additional config used by application. Contain tables with users, groups, etc. | 'mydb/appcommand.dat' |

---
### Application Core Settings - DO NOT MAKE CHANGES, THEY ARE THERE JUST FOR QoL
**!! Any changes will most likely break your software, not forever but for the time beeing. Please just skip this section, it is end of file anyway !!**

| Variable Name     | Default       | Format    | Description | Example |
|-------------------|---------------|-----------|-------------|---------|
| DevMode | False | Boolean | Turn on development mode. Change all log priority to DEBUG, turn on console logging and disable several bot options for testing purpose. | - |
| AppRoot | path.abspath(path.dirname(__file__)) | Method | Generate path location for main executable. Required for logging, versioning and hashes. | - |
| HostPlatform | 'linux' | - | Determine host machine operating system to optimize several methods. Variable is set by iterating system tags and finding proper one. | - |
| APP_Locale | LC_ALL | Locale | Set locale for all categories to the user’s default setting. LC_ALL as default is enough for majority of situations. | - |
