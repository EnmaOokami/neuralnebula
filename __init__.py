### Changelog:
#--------------------------- 0.0.1
# > Initial commit
#--------------------------- 0.0.2
# > Moved loggin as separated module
###

import logging
import discord
import socket
import os
import sys
import locale
from lib.cfg.app import APP_Locale
from collections import namedtuple

#
# Locale
locale.setlocale(APP_Locale, '')

#
# Info
__title__ = AppName
__author__ = 'Cryolite'
__license__ = 'MIT'
__copyright__ = 'Copyright 2020 Cryolite'
__version__ = '0.0.2-dev-0'

VersionInfo = namedtuple('VersionInfo', 'major minor micro releaselevel serial')
version_info = VersionInfo(major=0, minor=0, micro=2, releaselevel='dev', serial=0)